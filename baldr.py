'''
Created on Jan 26, 2012

@author: Eysteinn Helgason & Ragnar Larusson
'''
import getpass
import beda
import time
import os

if __name__ == '__main__':
    #f = open('clusters.txt','r')
    #lines = f.readlines()
    #f.close
    pw = getpass.getpass('keyfile password: ')
    hpcServer = []

    numNodes = 1
    runfile = 'runBeda'
    rd = 'drullumall' #remote directory
    case_dir = os.path.basename(os.getcwd())
   

    #for serverHost in lines:
    #    hpcServer.append(cluster.hpc(serverHost.strip(),pw))
    hpcServer.append(beda.hpc(rd, pw))
    
hpcServer[0].upload(rd,case_dir)  

jobId = hpcServer[0].submit(rd, runfile, case_dir)
print jobId
while hpcServer[0].jobStatus(jobId):
    hpcServer[0].prstat('running job ' + jobId)
    time.sleep(150)
hpcServer[0].prstatD('running job ' + jobId)
print os.path.basename(os.getcwd())
hpcServer[0].download(rd,case_dir)
hpcServer[0].test(rd)
        