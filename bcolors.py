'''
Created on Feb 15, 2012

@author: Eysteinn Helgason
'''

class bcolors(object):
    """Class that returns text colors for terminal, E is the normal black"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    GR = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    E = '\033[0m'
    BOLD = '\033[1m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.E = ''
        self.BOLD = ''
