'''
Created on Jan 26, 2012

@author: Eysteinn Helgason & Ragnar Larusson
'''

import os
import pysftp
import time
import sys
from bcolors import bcolors as bcol

class hpc(object):
    """Class with all commands modified for the beda at c3se Chalmers cluster"""
    
    host = 'beda.c3se.chalmers.se'
    def __init__(self, remoteDirectory, pw):
        """Test if the connection works and create remote directory for upploading files"""
        print '.... connecting to beda.c3se.chalmers.se\r', 
        sys.stdout.flush()
        self.pw = pw
        host = 'beda.c3se.chalmers.se'
        srv = pysftp.Connection(host=host, private_key_pass=pw)
        self.remoteHome = srv.getcwd()
        srv.execute('mkdir ' + remoteDirectory)
        srv.close()
        print bcol.GR + 'DONE' + bcol.E + ' connecting to beda.c3se.chalmers.se'
        
    def __call__(self):
        return self.host
    
    def name(self):
        return self.host
    
    def prstat(self, inpstatus):
        """Prints out a inpstatus message to the terminal led by a .... """
        print '.... ' + inpstatus + '\r',
        sys.stdout.flush()
    
    def prstatD(self,inpstatus):
        """Prints out DONE in green in front of inpstatus """
        print bcol.GR + "DONE" + bcol.E + ' ' + inpstatus
    
    def test(self, remoteDirectory):
        """test function to see if the connection works and prints out the remote directory"""
        srv = pysftp.Connection(host=self.host, private_key_pass=self.pw)
        srv.chdir(remoteDirectory)
        bla = srv.execute('pwd')
        print bla[0]
        srv.close()
    
    def upload(self,remoteDirectory, folderToUpload):
        """Uploads folderToUpload to remoteDirectory
           The files are compressed(tgz) and then transferred and extracted"""
        self.prstat('compressing files')
        sys.stdout.flush()
        os.chdir('../')
        os.system("tar -czf baldr"+ folderToUpload + ".tgz " + folderToUpload)
        srv = pysftp.Connection(host=self.host, private_key_pass=self.pw)
        srv.chdir(remoteDirectory)
        #srv.put('baldrUpload/baldrUpload_tmp.gz')
        self.prstatD('compressing files')
        self.prstat('uploading files')
        srv.put("baldr" + folderToUpload + ".tgz")
        srv.execute("tar -xzf "+ remoteDirectory +"/baldr" + folderToUpload + ".tgz -C ~/"+ remoteDirectory +"/")
        srv.execute("rm "+ remoteDirectory +"/baldr" + folderToUpload + ".tgz")
        srv.close()
        os.chdir(folderToUpload)
        self.prstatD('uploading files')
    
    def download(self, remoteDirectory, folderToDownload):
        """Downloads folderToDownload from remoteDirectory
           The files are compressed(tgz) transferred and extracted locally"""
        self.prstat('downloading files')
        time.sleep(0)
        srv = pysftp.Connection(host=self.host, private_key_pass=self.pw)
        #srv.chdir('baldr')
        srv.execute('tar -czf ' + remoteDirectory+'/baldr'+ folderToDownload + '.tgz -C ~/' + remoteDirectory+'/ ' + folderToDownload)
        srv.get(remoteDirectory + '/baldr'+ folderToDownload + '.tgz', '../baldr'+ folderToDownload + '.tgz')
        srv.close()
        os.chdir('../')
        os.system('tar -xzf baldr'+ folderToDownload + '.tgz')
        self.prstatD('downloading files')
        
    def jobStatus(self , jobId):
        """Returns 1 if job with job id = jobId is in the que or running else 0"""
        self.status = 1
        srv = pysftp.Connection(host=self.host, private_key_pass=self.pw)
        jobStatus = srv.execute('qstat ' + jobId)
        if "qstat: Unknown Job Id " in jobStatus[0]:
            self.status = 0
        srv.close()
        return self.status
            
    def submit(self, remoteDirectory, runfile,case_dir):
        """Gets all possible projects numbers and submits the runfile in the remoteDirectory
           picks out the project number with the shortest start time and runs that job"""
        srv = pysftp.Connection(host=self.host, private_key_pass=self.pw) # connect to cluster
        #srv.get('baldr/' + case_dir + '/' + runfile , case_dir + '/' + runfile)                        # get the runfile from cluster
        srv.get(remoteDirectory + '/' + case_dir + '/' + runfile, runfile)                        # get the runfile from cluster
        self.runfile = runfile
        #r = open(case_dir + '/' + self.runfile,'r')                                # opens runfile
        r = open(self.runfile,'r')                                # opens runfile
        runlines = r.readlines()                                            # collect info from runfile
        r.close                                                             #closes runfile
#       runlines.insert(0,'cd ' + 'baldr/' + case_dir + '/')
#       runlines[30] = 'cd ' + 'baldr/' + case_dir + '/ \n'
        projsign = "#PBS -A "    # line starts with this in runscript if a prject has been selected
        projsearch = []         # Here we put in the search results when we search for if a line includes the projsign
        
        projstring = 0          # used to check if project was selected or not. If no project was selected this remains unchanged
        for i in range(0,len(runlines)):        # search for if a project has been selected in runfile (if any line gives 0 then a project has been selected)
            projsearch.append(runlines[i].find(projsign))           # Fills in search results into projsearch
            if projsearch[i] == 0:                                  # checks if project has been selected
                projstring = runlines[i]
                print '\n'                         # Collects what project was already selected in the runfile.
                print 'project ' + projstring[5:20] + ' was already selected' # informs the user what project had already been specified in the runfile.
             
        if projstring == 0:    # After the search in the above for loop we check if no project had been selected.
            
            print 'No project has been selected. \nA project will be selected for you and there is nothing you can do about it.'  
            projinfo = srv.execute('projinfo')  # executes the projinfo command on beda to that prints information about projects and cpu hours amongst other data.
            avail_proj = []                     # We will list the available prject number in this arry later. 
            avail_projsign = '-----'            # We search for this in the output from the projinfo command because we know that the next line contains the project number.
            avail_projsearch = []               # Here we put in the search results when we search for avail_projsign
            
            for j in range(0,len(projinfo)):    # no we look for the available project numbers
                avail_projsearch.append(projinfo[j].find(avail_projsign)) # and list the results
                if avail_projsearch[j] == 0:                                # check if we found avail_projsign
                    avail_proj_line = projinfo[j+1]                         # we store the line below the line containing avail_projsign
                    avail_proj.append(avail_proj_line.split()[0])           # and filter out the first word in that line since that word is the project number and we store it in avail_proj
            print '\n'
            print 'Here are the projects that are available to you:'        
            print avail_proj
            print 'Now I am going to select the best project for you. That is how nice I am!'
            
            projTime = []    # this is where we store the queue times for the project in the format that projinfo gives.
            # this time vector has the format dd:hh:mm:ss
            minTime = 9999999999
            for k in range(0,len(avail_proj)):  # This loop submits the job for each available project, collects the queue time and then kills the job.
                #srv.get('baldr/' + case_dir + '/' + runfile , case_dir + '/' + runfile)     # get the runfile from cluster
                srv.get(remoteDirectory + '/' + case_dir + '/' + runfile)     # get the runfile from cluster
                runlines[1] = projsign + avail_proj[k] + "\n"                      # put in new project number into runlines
                
                #f = open(case_dir + '/' + self.runfile,'w')                         # write a new runfile
                f = open(self.runfile,'w')                         # write a new runfile
                for ii in range(0,len(runlines)):
                    f.write(runlines[ii])              
                f.close()                

                #srv.put(case_dir + '/' + self.runfile, 'baldr/' + case_dir + '/' + self.runfile)  # return runfile to cluster
                srv.put(self.runfile, remoteDirectory + '/' + case_dir + '/' + self.runfile)  # return runfile to cluster
                qsubReturn = srv.execute('cd ' + remoteDirectory + '/' + case_dir + '/ && qsub ' + self.runfile)  # submits job on cluster
                job_id = qsubReturn[0].partition('.')[0]
                time.sleep(1)
                
                showstart = srv.execute('showstart ' + job_id)
                time_line = showstart[1].split()               # we split the line in showstart into "words"
                projTime.append(time_line[3])    
                print avail_proj[k]  + ' ' + time_line[3]   
                if time_line[3] == "00:00:00":              # if there is project that starts immediately,
                    print "Found project that starts now"   # just continue with that
                    return job_id                           # and return the job_id
             
                tempTime2 = time_line[3].replace(":", "")
                if tempTime2 == 'job':
                    temp_time = 99999999
                else:
                    temp_time = int(tempTime2)
                if temp_time < minTime:
                    minTime = temp_time
                    minProj = avail_proj[k]
                
                srv.execute('qdel ' + job_id) # now we kill the time because we do not want to influnce the queue with this project when we look for the queue time for the nex project
                
            print '\n The project with the shortest queueing time is ' + minProj
            
            #srv.get('baldr/' + case_dir + '/' + runfile , case_dir + '/' + runfile)     # get the runfile from cluster
            srv.get(remoteDirectory + '/' + case_dir + '/' + runfile)     # get the runfile from cluster
            runlines[1] = projsign + minProj + "\n"                      # put in new project number into runlines
                
            #f = open(case_dir + '/' + self.runfile,'w')                         # write a new runfile
            f = open(self.runfile,'w')                         # write a new runfile
            for ii in range(0,len(runlines)):
                f.write(runlines[ii])              
            f.close()                

            #srv.put(case_dir + '/' + self.runfile, 'baldr/' + case_dir + '/' + self.runfile)  # return runfile to cluster
            srv.put(self.runfile, remoteDirectory + '/' + case_dir + '/' + self.runfile)  # return runfile to cluster
        qsubReturn = srv.execute('cd ' + remoteDirectory + '/' + case_dir + '/ && qsub ' + self.runfile)  # submits job on cluster
        job_id = qsubReturn[0].partition('.')[0]
        srv.close()   # close connection to cluster
                            
        print '\n Submit complete'
        return job_id
        
        

if __name__ == "__main__":
    print "Run baldr.py"
        